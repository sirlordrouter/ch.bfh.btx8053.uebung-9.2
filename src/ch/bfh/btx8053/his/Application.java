package ch.bfh.btx8053.his;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Application {

	  private static final String PERSISTENCE_UNIT_NAME = "his";
	  private static EntityManager em;
	  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 em = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
		
		 
		 /* --------------------------------------------------------------------- */
		 /*     ACHTUNG!!!!!!!!!!!! 								            
		  *		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		  *		(Otherwise you will have problems in your database. Solution: Delete HSQDB Files *.script, *.log )
		 /* Comment following out when run a second time (comment out until "END")*/
		
		Staff staff1 = new Staff("Emma", "Watson", new Address("Mantelgasse 1", "1234", "Hollywood"), 
				"123", "Chief of Interest", "Doctor");
		
		Staff staff2 = new Staff("Sherlok", "Holmes", new Address("Baker Street", "", "London"), 
				"456", "Master of Desaster", "Nurse");
		
		Staff staff3 = new Staff("Earl", "Grey", new Address("Weg", "127001", "Localhost"), 
				"789", "Chief of Interest", "Assistant");
		
		Patient p1 = new Patient("Hansi","Hinterseer",
			new Address("Beispielstrasse 1", "3000","Entenhausen"),
			new Insurance("Helsana","KVG",
					new Address("Langstrasse", "1111", "Langnau")));
					
		
		PatientCase p1case = new PatientCase(
			p1,
			new GregorianCalendar(2014, 0, 24),
			"Beinbruch", 
			new GregorianCalendar(2014, 0, 28));
		
		ArrayList<Diagnosis> p1caseDiagnosis = new ArrayList<>();
		Diagnosis p1Case1Diagnosis = new Diagnosis(
				new GregorianCalendar(2014, 0, 24), "distale Tibiafraktur links");
		p1Case1Diagnosis.setPatientCase(p1case);
		p1Case1Diagnosis.setStaff(
				new ArrayList<Staff>(
						Arrays.asList(staff1,staff2,staff3)));
		p1Case1Diagnosis.setTreatments(
			new ArrayList<Treatment>(
				Arrays.asList(
					new Treatment(
							new GregorianCalendar(2014, 0, 24), 12, 
							"Ostesynthese",false,p1Case1Diagnosis,
							new ArrayList<Staff>(Arrays.asList(staff1))
						)
				)
			)
		);
		
		p1caseDiagnosis.add(p1Case1Diagnosis);
		
		Diagnosis p1Case2Diagnosis = new Diagnosis(
				new GregorianCalendar(2014, 0, 24), "laterale Malearfraktur links");
		
		p1Case2Diagnosis.setPatientCase(p1case);
		p1Case2Diagnosis.setStaff(
				new ArrayList<Staff>(
						Arrays.asList(staff1,staff2,staff3)));
		p1Case2Diagnosis.setTreatments(
			new ArrayList<Treatment>(
				Arrays.asList(
					new Treatment(
							new GregorianCalendar(2014, 0, 25), 30, 
							"Gips",false,p1Case2Diagnosis,
							new ArrayList<Staff>(Arrays.asList(staff3))
						)
				)
			)
		);
		
		p1caseDiagnosis.add(p1Case2Diagnosis);
		
		p1case.setDiagnosis(p1caseDiagnosis);
		p1.setCases(new ArrayList<PatientCase>(Arrays.asList(p1case)));
		
		System.out.println(p1);
		
	    EntityTransaction transaction = em.getTransaction();
	    transaction.begin();
	    em.persist(p1case);
	    
	    em.flush();
	    transaction.commit();
	    
		 /* END */
		 
		 
		 /* Query for the stored Cases*/
		 /*REMARK: Data is not fetched from the database when method toString is invoked. 
		  * The data is loaded lazy, so any other method like size(), first() will fetch all the data
		  * from the database. */
		 
		 Query caseQUERY = em.createQuery("select c from PatientCase c");
		 @SuppressWarnings("unchecked")
		List<PatientCase> cases= caseQUERY.getResultList();
		 
		 for (PatientCase patientCase : cases) {
			 List<Diagnosis> d = patientCase.getDiagnosis();
			 
			 for (Diagnosis diagnosis : d) {
				System.out.println(diagnosis);
				
				for (Treatment t : diagnosis.getTreatments()) {
					System.out.println(t);
				}
			}
		}
		 
		 /* Leave this for correct shutdown*/
		 
		 EntityTransaction transactionSHutdown = em.getTransaction();
		 transactionSHutdown.begin();
		  Query q = em.createNativeQuery("SHUTDOWN COMPACT");
		  q.executeUpdate();
		  em.flush();
		  transactionSHutdown.commit();	  
		  
		    // It is always good practice to close the EntityManager so that resources
		    // are conserved.
		    em.close();
		    
		    
		    
	    
	}

}
