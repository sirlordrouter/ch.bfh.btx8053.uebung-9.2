package ch.bfh.btx8053.his;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Diagnosis {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Temporal(TemporalType.DATE)
	private GregorianCalendar date;
	private String description;
	
	@ManyToOne
	private PatientCase patientCase;
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	private List<Staff> staff = new ArrayList<Staff>();
	@OneToMany(mappedBy="diagnosis", cascade=CascadeType.PERSIST)
	private List<Treatment> treatments = new ArrayList<>();
	
	
	public Diagnosis() {}
	
	public Diagnosis(GregorianCalendar date, String description) {
		this.date = date;
		this.description = description;
	}
	
	public Diagnosis(GregorianCalendar date, String description,
			PatientCase patientCase, ArrayList<Staff> staff,
			ArrayList<Treatment> treatments) {
		super();
		this.date = date;
		this.description = description;
		this.patientCase = patientCase;
		this.staff = staff;
		this.treatments = treatments;
	}

	public GregorianCalendar getDate() {
		return date;
	}
	
	public void setDate(GregorianCalendar date) {
		this.date = date;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the patientCase
	 */
	public PatientCase getPatientCase() {
		return patientCase;
	}

	/**
	 * @param patientCase the patientCase to set
	 */
	public void setPatientCase(PatientCase patientCase) {
		this.patientCase = patientCase;
	}

	/**
	 * @return the staff
	 */
	public List<Staff> getStaff() {
		return staff;
	}

	/**
	 * @param staff the staff to set
	 */
	public void setStaff(List<Staff> staff) {
		this.staff = staff;
	}

	/**
	 * @return the treatments
	 */
	public List<Treatment> getTreatments() {
		return treatments;
	}

	/**
	 * @param treatments the treatments to set
	 */
	public void setTreatments(List<Treatment> treatments) {
		this.treatments = treatments;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Diagnosis [id=" + id + ", date=" + date + ", description="
				+ description + "]";
	}
	
	
}
