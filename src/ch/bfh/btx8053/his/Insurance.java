package ch.bfh.btx8053.his;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Insurance {
	
	@Id
	@GeneratedValue
	private int id;
	
	private String name;
	private String type;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	private Address address;
	
	public Insurance() {}

	public Insurance(String name, String type, Address address) {
		this.name = name;
		this.type = type;
		this.address = address;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Insurance [id=" + id + ", name=" + name + ", type=" + type
				+ ", address=" + address + "]";
	}
	
	
}
