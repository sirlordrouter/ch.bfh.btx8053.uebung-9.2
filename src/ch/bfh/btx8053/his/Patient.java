package ch.bfh.btx8053.his;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("PATIENT")
public class Patient extends Person {
	

	@OneToOne(cascade=CascadeType.PERSIST)
	private Insurance insurance;
	
	@OneToMany(mappedBy="patient")
	private List<PatientCase> cases = new ArrayList<PatientCase>();
	
	public Patient( ) {super(); }

	public Patient(String firstName, String lastName, Address address, Insurance insurance) {
		super (firstName, lastName, address);
		this.address = address;
		this.insurance = insurance;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}	
	
	/**
	 * @return the cases
	 */
	public ArrayList<PatientCase> getCases() {
		return (ArrayList<PatientCase>) cases;
	}
	
	/**
	 * @param cases the cases to set
	 */
	public void setCases(ArrayList<PatientCase> cases) {
		this.cases = cases;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Patient [insurance=" + insurance + ", cases=" + cases + "]";
	}
	
	
}
