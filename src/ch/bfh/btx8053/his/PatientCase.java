package ch.bfh.btx8053.his;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PatientCase {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Patient patient;
	
	@Temporal(TemporalType.DATE)
	private GregorianCalendar startDate;
	private String startDescription;
	@Temporal(TemporalType.DATE)
	private GregorianCalendar endDate;
	
	@OneToMany(mappedBy="patientCase", cascade=CascadeType.PERSIST)
	private List<Diagnosis> diagnosis = new ArrayList<Diagnosis>();
	
	public PatientCase() {	}
	
	public PatientCase(Patient patient, GregorianCalendar startDate,
			String startDescription, GregorianCalendar endDate) {
		super();
		
		this.patient = patient;
		this.startDate = startDate;
		this.startDescription = startDescription;
		this.endDate = endDate;
	}
	
	public PatientCase(Patient patient, GregorianCalendar startDate,
			String startDescription, GregorianCalendar endDate,
			List<Diagnosis> diagnosis) {		
		this(patient,startDate,startDescription,endDate);
		this.diagnosis = diagnosis;
	}

	public void setStart(GregorianCalendar date, String description) {
		this.startDate = date;
		this.startDescription = description;
	}
	
	public void setEnd(GregorianCalendar date) {
		this.endDate = date;
	}

	public GregorianCalendar getStartDate() {
		return startDate;
	}

	public String getStartDescription() {
		return startDescription;
	}

	public GregorianCalendar getEndDate() {
		return endDate;
	}

	/**
	 * @return the diagnosis
	 */
	public List<Diagnosis> getDiagnosis() {
		return diagnosis;
	}

	/**
	 * @param diagnosis the diagnosis to set
	 */
	public void setDiagnosis(List<Diagnosis> diagnosis) {
		this.diagnosis = diagnosis;
	}

	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(GregorianCalendar startDate) {
		this.startDate = startDate;
	}

	/**
	 * @param startDescription the startDescription to set
	 */
	public void setStartDescription(String startDescription) {
		this.startDescription = startDescription;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(GregorianCalendar endDate) {
		this.endDate = endDate;
	}
	
	
}
