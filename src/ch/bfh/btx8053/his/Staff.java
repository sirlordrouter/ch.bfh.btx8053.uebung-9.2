package ch.bfh.btx8053.his;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("STAFF")
public class Staff extends Person {
	
	private String phone;
	private String position;
	private String function;
	

	public Staff() {super();}
	
	public Staff(String firstName, String lastName, Address address, String phone, String position, String function) {
		super (firstName, lastName, address);
		this.phone = phone;
		this.position = position;
		this.function = function;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	public String getFunction() {
		return function;
	}
	
	public void setFunction(String function) {
		this.function = function;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Staff [phone=" + phone + ", position=" + position
				+ ", function=" + function + "]";
	}
}
