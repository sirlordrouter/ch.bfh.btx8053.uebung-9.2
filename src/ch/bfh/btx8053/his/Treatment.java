package ch.bfh.btx8053.his;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Treatment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Temporal(TemporalType.DATE)
	private GregorianCalendar date;
	private int duration; // in minutes
	private String description;
	private boolean invoiced = false;
	
	@ManyToOne
	private Diagnosis diagnosis;
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	private List<Staff> staff = new ArrayList();
	
	
	public Treatment() {}
	
	public Treatment(GregorianCalendar date, int duration, String description,
			boolean invoiced, Diagnosis diagnosis, ArrayList<Staff> staff) {
		super();
		this.date = date;
		this.duration = duration;
		this.description = description;
		this.invoiced = invoiced;
		this.diagnosis = diagnosis;
		this.staff = staff;
	}

	public Treatment(GregorianCalendar date, String description) {
		this.date = date;
		this.description = description;
	}

	public GregorianCalendar getDate() {
		return date;
	}
	
	public void setDate(GregorianCalendar date) {
		this.date = date;
	}
	
	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isInvoiced() {
		return invoiced;
	}

	public void setInvoiced(boolean invoiced) {
		this.invoiced = invoiced;
	}

	/**
	 * @return the diagnosis
	 */
	public Diagnosis getDiagnosis() {
		return diagnosis;
	}

	/**
	 * @param diagnosis the diagnosis to set
	 */
	public void setDiagnosis(Diagnosis diagnosis) {
		this.diagnosis = diagnosis;
	}

	/**
	 * @return the staff
	 */
	public ArrayList<Staff> getStaff() {
		return (ArrayList<Staff>) staff;
	}

	/**
	 * @param staff the staff to set
	 */
	public void setStaff(ArrayList<Staff> staff) {
		this.staff = staff;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Treatment [id=" + id + ", date=" + date + ", duration="
				+ duration + ", description=" + description + ", invoiced="
				+ invoiced + ", diagnosis=" + diagnosis + ", staff=" + staff
				+ "]";
	}	
	
	
}
